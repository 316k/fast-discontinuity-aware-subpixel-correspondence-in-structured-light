CC      := gcc
CCFLAGS := -std=c99 -Wall -g -fopenmp
LDFLAGS := -lm
TARGETS := generate solve subpixel-random subpixel-gradient translation naive-discontinuity
MAINS   := $(addsuffix .o, $(TARGETS) )
OBJ     := lodepng.o helpers.o $(MAINS)
DEPS    := lodepng.h helpers.h


.PHONY: all clean

all: $(TARGETS)

clean:
	rm -f $(TARGETS) $(OBJ)
	rm -rf test/

$(OBJ): %.o : %.c $(DEPS)
	$(CC) -c -o $@ $< $(CCFLAGS)

solve: lodepng.o helpers.o solve.o
	$(CC) -o $@ $^ $(CCFLAGS) $(LDFLAGS) -lgmp

$(filter-out solve, $(TARGETS)): % : $(filter-out $(MAINS), $(OBJ)) %.o
	$(CC) -o $@ $^ $(CCFLAGS) $(LDFLAGS)
