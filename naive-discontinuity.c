/*
  Compute a discontinuity map by counting every pixels with a
  non-neighbour

  A neighbour is is pixel with a match within the correlated
  neighbourhood (which depends on the pattern frequency)
*/
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "args.h"
#include "helpers.h"

#define MIN(a, b) (a < b) ? (a) : (b)
#define MAX(a, b) (a > b) ? (a) : (b)

int from_w, from_h, to_w, to_h;

int main(int argc, char** argv) {

    int nthreads = 4;
    int verbose = 0;
    int discontinuity_threshold = 1;

    char* output_fname = "naive-discontinuity-map.png";
    char* lut_fname = "lutCamPixel.png";

    // Args parsing
    ARGBEGIN
        ARG_CASE('t')
        nthreads = ARGI;

    ARG_CASE('v')
        verbose = 1;

    ARG_CASE('O')
        output_fname = ARGS;

    ARG_CASE('L')
        lut_fname = ARGS;

    LARG_CASE("discontinuity-threshold")
        discontinuity_threshold = ARGI;

    WRONG_ARG
        printf("usage: %s [-t nb_threads=%d]\n"
               "\t[-O output=%s] [-L lut=%s]\n"
               "\t[--discontinuity-threshold=%d]\n",
               argv0, nthreads, output_fname, lut_fname,
               discontinuity_threshold
            );
        exit(1);

    ARGEND

    omp_set_num_threads(nthreads);

    srand(time(NULL));

    // Check file size to avoid problems if patterns.txt is empty
    FILE* info = fopen("patterns.txt", "r");

    if(info != NULL)
        fseek(info, 0, SEEK_END);

    if(info == NULL || !ftell(info)) {
        printf("error: empty patterns.txt\n");
        exit(-1);
    }
    fseek(info, 0, SEEK_SET);

    int foo;
    fscanf(info, "%d %d %d",
           &to_w, &to_h, &foo);

    fclose(info);

    // Read an image to get from_w & from_h
    free_f32cube(load_color(lut_fname, &from_w, &from_h), 3);

    // Load pixel-precise LUT
    float*** matches = load_color(lut_fname, &from_w, &from_h);

    #pragma omp parallel for
    for(int i=0; i<from_h; i++)
        for(int j=0; j<from_w; j++) {

            if(matches[X][i][j] == 65535.0) {
                matches[X][i][j] = matches[Y][i][j] = matches[DIST][i][j] = -1.0;
            } else {
                matches[X][i][j] = round(matches[X][i][j] / 65535.0 * (to_w - 1));
                matches[Y][i][j] = round(matches[Y][i][j] / 65535.0 * (to_h - 1));
                matches[DIST][i][j] = matches[DIST][i][j] / 65535.0;
            }
        }

    float** discontinuity_map = malloc_f32matrix(from_w, from_h);

    // Fill with zeros
    #pragma omp parallel for
    for(int i=0; i<from_h; i++) {
        for(int j=0; j<from_w; j++) {
            discontinuity_map[i][j] = 0;
        }
    }

    int progress_bar_increment = from_h / 50;

    if(verbose && progress_bar_increment) {
        // Progress-bar
        for(int i=0; i<from_h; i += progress_bar_increment) {
            fprintf(stderr, ".");
        }
        fprintf(stderr, "\n");
    }

    long last = time(NULL);

    #pragma omp parallel for
    for(int i=0; i<from_h; i++) {

        if(verbose && progress_bar_increment && i % progress_bar_increment == 0)
            fprintf(stderr, ".");

        for(int j=0; j<from_w; j++) {

            int match_x = (int) matches[X][i][j];
            int match_y = (int) matches[Y][i][j];

            if(match_x == -1 || // Unmatched pixel
               // Border
               i == 0 || i == from_h - 1 ||
               j == 0 || j == from_w - 1 ||
               // Border
               match_x <= 0 || match_x >= to_w - 1 ||
               match_y <= 0 || match_y >= to_h - 1) {
                discontinuity_map[i][j] = 255;
                continue;
            }

            int is_discontinuity = 0;
            int unmatched_neighbours = 0;
            int min_x = matches[X][i][j], max_x = matches[X][i][j];
            int min_y = matches[Y][i][j], max_y = matches[Y][i][j];

            for(int y=i-1; y<=i+1; y++) {
                for(int x=j-1; x<=j+1; x++) {

                    if((i == y && j == x) ||
                       x < 0 || y < 0 ||
                       x >= from_w || y >= from_h) {
                        continue;
                    }

                    int match_x = matches[X][y][x];

                    if(match_x == -1) {
                        unmatched_neighbours++;
                        continue;
                    }

                    int match_y = matches[Y][y][x];

                    min_x = MIN(min_x, match_x);
                    max_x = MAX(max_x, match_x);

                    min_y = MIN(min_y, match_y);
                    max_y = MAX(max_y, match_y);
                }
            }

            is_discontinuity = unmatched_neighbours == 8 ||
                (max_x - min_x) > discontinuity_threshold ||
                (max_y - min_y) > discontinuity_threshold;

            if(is_discontinuity)
                discontinuity_map[i][j] = 255;
        }
    }

    if(verbose) {
        putchar('\n');
        printf("time: %ld\n", time(NULL) - last);
    }

    // Write subpixel-precise LUT
    save_gray_png(
        output_fname,
        discontinuity_map, from_w, from_h, 8);

    return EXIT_SUCCESS;
}
