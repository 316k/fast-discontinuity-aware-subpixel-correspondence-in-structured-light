/*
  Compute camera-projector LUTs from matching patterns/pictures
*/
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <gmp.h>

#include "args.h"
#include "helpers.h"

// --- Global variables ---
// Input-related parameters
int nb_patterns, from_w, from_h, to_w, to_h, skip = 1, nb_bits;
int verbose = 0;

int nb_selected_bits = -1;

size_t hash_table_size;

// Threshold to consider a pixel as part of the scanned zone
// -1 == disable mask, hash all pixels
float mask_threshold = 20;
float** mask;

long time_since_start;

#define HASH_TO_CODES   hash_to_codes(to_codes, hash_table, bits_used, i, j, &nb_collisions)

#define HASH_FROM_CODES hash_from_codes(matches, from_codes, to_codes, \
                                          hash_table, bits_used, i, j, \
                                          &nb_new_matches, &nb_better_matches)

// ----- Datastructures using GMP -----

mpz_t** malloc_mpz_matrix(int w, int h, int nb_bits) {
    mpz_t** matrix = malloc(sizeof(mpz_t*) * h);
    matrix[0] = (mpz_t*) calloc(h * w, sizeof(mpz_t));

    for(int i=0; i < h; i++) {
        matrix[i] = (*matrix + w * i);

        for(int j=0; j<w; j++) {
            mpz_init2(matrix[i][j], nb_bits);
        }
    }

    return matrix;
}

void free_mpz_matrix(mpz_t** matrix) {
    free(matrix[0]);
    free(matrix);
}

mpz_t** load_binary_codesq(char* format, int width, int height, int nb_patterns) {

    char filename[FNAME_MAX_LEN];
    int w, h, depth;
    int nb_bits = nb_patterns * (nb_patterns - 1) / 2;

    unsigned char** intensities = malloc(sizeof(unsigned char*) * nb_patterns);

    for(int i=0; i<nb_patterns; i++) {
        sprintf(filename, format, i);
        intensities[i] = load_png(filename, &w, &h, &depth, LCT_GREY);
        assert(depth == 8);
    }

    mpz_t** bits = malloc_mpz_matrix(width, height, nb_bits);

    int k = 0;
    for(int n1=0; n1 < nb_patterns - 1; n1++) {
        for(int n2=n1+1; n2 < nb_patterns; n2++) {

            int idx = 0;
            for(int i=0; i<height; i++) {
                for(int j=0; j<width; j++) {
                    int diff = intensities[n1][idx] - intensities[n2][idx] < 0;

                    if(diff) {
                        mpz_setbit(bits[i][j], k);
                    }

                    idx++;
                }
            }

            k++;
        }
    }

    for(int i=0; i<nb_patterns; i++) {
        free(intensities[i]);
    }
    free(intensities);

    return bits;
}

/**
 * Load linear binary codes from images
 */
mpz_t** load_binary_codesl(char* format,
                           int width, int height, int nb_patterns) {

    mpz_t** codes = malloc_mpz_matrix(width, height, nb_patterns - 1);

    char filename[FNAME_MAX_LEN];

    int w, h;

    // Load first cam image
    float** previous_image;

    sprintf(filename, format, 0);
    previous_image = load_gray(filename, &w, &h);

    assert(w == width && h == height);

    // Camera binary codes
    for(int k=0; k<nb_patterns - 1; k++) {

        float** current_image;

        // Load next image
        sprintf(filename, format, k + 1);
        current_image = load_gray(filename, &w, &h);

        assert(w == width && h == height);

        for(int i=0; i<height; i++)
            for(int j=0; j<width; j++) {

                int diff = (current_image[i][j] - previous_image[i][j]) < 0;

                if(diff) {
                    mpz_setbit(codes[i][j], k);
                }
            }

        free_f32matrix(previous_image);
        previous_image = current_image;
    }

    // Free last image
    free_f32matrix(previous_image);

    return codes;
}


void hash_to_codes(mpz_t** to_codes, int* hash_table[2], int* bits_used,
                   int i, int j, int* nb_collisions) {
    size_t hash = 0;

    for(int k=0; k < nb_selected_bits; k++) {
        hash |= mpz_tstbit(to_codes[i][j], bits_used[k]) << k;
    }

    // First there gets the match
    if(hash_table[X][hash] == -1) {
        hash_table[X][hash] = j;
        hash_table[Y][hash] = i;
    } else {
        (*nb_collisions)++;
    }
}

int spatial_propagation(float*** matches, float*** from_codes, float*** to_codes) {

    int nb_better_matches = 0;

    for(int iteration=0; iteration<2; iteration++) {

        int delta = iteration * 2 - 1; // -1 or +1

        int i_start = 0, i_end = from_h - 1, i_inc = +1;
        int j_start = 0, j_end = from_w - 1, j_inc = +1;

        if(iteration == 0) {
            i_start = from_h - 1;
            i_end = 0;
            i_inc = -1;

            j_start = from_w - 1;
            j_end = 0;
            j_inc = -1;
        }

        for(int i=i_start; i != i_end; i += i_inc)
            for(int j=j_start; j != j_end; j += j_inc) {

                // Skip thresholded pixels
                if(mask_threshold != -1 && mask[i][j] <= mask_threshold)
                    continue;

                // Process current
                int match_x = matches[X][i][j];

                // Don't try to propagate unassigned pixels
                if(match_x == -1)
                    continue;

                int match_y = matches[Y][i][j];
                // float current_cost = matches[DIST][i][j];

                // Don't propagate outside the images
                if(!(j + delta < from_w && match_x + delta < to_w &&
                     j + delta >= 0 && match_x + delta >= 0 &&
                     i + delta < from_h && match_y + delta < to_h &&
                     i + delta >= 0 && match_y + delta >= 0)) {
                    continue;
                }

                // Propagate horizontally
                if(mask_threshold == -1 || mask[i][j + delta] > mask_threshold) {

                    // Match by copying Neighbor <-> Current
                    float copy_cost = pixel_cost(
                        from_codes[i][j + delta],
                        to_codes[match_y][match_x],
                        nb_patterns);

                    // Match by propagating Neighbor <-> Neighbor
                    float propagation_cost = pixel_cost(
                        from_codes[i][j + delta],
                        to_codes[match_y][match_x + delta],
                        nb_patterns);

                    int to_x = match_x + delta;
                    int to_y = match_y;
                    float to_cost = propagation_cost;

                    // Choose best of copy/propagation
                    if(copy_cost < propagation_cost) {
                        to_x = match_x;
                        to_cost = copy_cost;
                    }

                    // Current match cost pour le voisin
                    float prev_neighbour_cost = matches[DIST][i][j + delta];

                    // If the neighbor is unmatched or is already
                    // matched with a higher cost, replace it
                    if(prev_neighbour_cost == -1.0 || to_cost < prev_neighbour_cost) {
                        nb_better_matches++;

                        matches[X][i][j + delta] = to_x;
                        matches[Y][i][j + delta] = to_y;
                        matches[DIST][i][j + delta] = to_cost;
                    }
                }

                // Propagate vertically
                if(mask_threshold == -1 || mask[i + delta][j] > mask_threshold) {

                    // Match by copying Neighbor <-> Current
                    float copy_cost = pixel_cost(
                        from_codes[i + delta][j],
                        to_codes[match_y][match_x],
                        nb_patterns);

                    // Match by propagating Neighbor <-> Neighbor
                    float propagation_cost = pixel_cost(
                        from_codes[i + delta][j],
                        to_codes[match_y + delta][match_x],
                        nb_patterns);

                    int to_x = match_x;
                    int to_y = match_y + delta;
                    float to_cost = propagation_cost;

                    // Choose best of copy/propagation
                    if(copy_cost < propagation_cost) {
                        to_y = match_y;
                        to_cost = copy_cost;
                    }

                    // Current match cost pour le voisin
                    float prev_neighbour_cost = matches[DIST][i + delta][j];

                    // If the neighbor is unmatched or is already
                    // matched with a higher cost, replace it
                    if(prev_neighbour_cost == -1.0 || to_cost < prev_neighbour_cost) {
                        nb_better_matches++;

                        matches[X][i + delta][j] = to_x;
                        matches[Y][i + delta][j] = to_y;
                        matches[DIST][i + delta][j] = to_cost;
                    }
                }
            }
    }

    if(verbose)
        printf("Improved matches : %d\n", nb_better_matches);

    return nb_better_matches;
}

void hash_from_codes(float*** matches, mpz_t** from_codes, mpz_t** to_codes,
                     int* hash_table[2], int* bits_used, int i, int j,
                     int* nb_new_matches, int* nb_better_matches) {

    size_t hash = 0;

    for(int k=0; k < nb_selected_bits; k++) {

        hash |= mpz_tstbit(from_codes[i][j], bits_used[k]) << k;

    }

    // Collision = match
    if(hash_table[X][hash] != -1) {
        int x = hash_table[X][hash];
        int y = hash_table[Y][hash];

        int distance = mpz_hamdist(from_codes[i][j], to_codes[y][x]);

        // If the new matching cost is smaller, update
        if(matches[DIST][i][j] == -1.0 || distance < matches[DIST][i][j]) {
            if(matches[DIST][i][j] == -1.0)
                (*nb_new_matches)++;
            else
                (*nb_better_matches)++;

            matches[X][i][j] = x;
            matches[Y][i][j] = y;
            matches[DIST][i][j] = distance;
        }
    }
}

/**
 * Peforms one iteration of the LSH algorithm.
 *
 * This function is NOT thread-safe and should NOT be called multiple
 * times in parallel. All the parallel work is done from inside the
 * function.
 */
void lsh(float*** matches, mpz_t** from_codes, mpz_t** to_codes) {

    int nb_collisions = 0, nb_new_matches = 0, nb_better_matches = 0;

    hash_table_size = (size_t) 1 << nb_selected_bits;

    if(verbose) {
        printf("nb_selected_bits=%d\n", nb_selected_bits);
        printf("hash_table_size=%d (%f times bigger than %d)\n",
               (int) hash_table_size, hash_table_size /(float)(to_w * to_h),
               to_w * to_h);
    }

    int* hash_table[2];

    for(int i=0; i<2; i++) {
        hash_table[i] = malloc(hash_table_size * sizeof(int));

        for(int j=0; j<hash_table_size; j++)
            hash_table[i][j] = -1;
    }

    int* bits_used = random_numbers(nb_selected_bits, nb_bits);

    int iteration = rand() % 4;

    // Hash des to_codes
    switch(iteration) {
    case 0:
        #pragma omp parallel for
        for(int i=0; i < to_h; i++)
            for(int j=0; j < to_w; j++)
                HASH_TO_CODES;
        break;

    case 1:
        #pragma omp parallel for
        for(int i=0; i < to_h; i++)
            for(int j=to_w - 1; j >= 0; j--)
                HASH_TO_CODES;
        break;

    case 2:
        #pragma omp parallel for
        for(int i=to_h - 1; i >= 0; i--)
            for(int j=0; j < to_w; j++)
                HASH_TO_CODES;
        break;

    case 3:
        #pragma omp parallel for
        for(int i=to_h - 1; i >= 0; i--)
            for(int j=to_w - 1; j >= 0; j--)
                HASH_TO_CODES;
        break;
    }

    if(verbose) {
        printf("nb_collisions: %d / %d = %f%%\n", nb_collisions, to_w * to_h,
               nb_collisions /(float)(to_w * to_h) * 100);

        printf("hash_space_used: %d / %d = %f%%\n", to_w * to_h - nb_collisions,
               (int) hash_table_size,
               (to_w * to_h - nb_collisions)/(float)hash_table_size * 100);
    }

    // from_codes hashing (+ updating matches)
    switch(iteration) {
    case 0:
        #pragma omp parallel for
        for(int i=0; i < from_h; i++)
            for(int j=0; j < from_w; j++)
                if(mask_threshold == -1 || mask[i][j] > mask_threshold)
                    HASH_FROM_CODES;
        break;

    case 1:
        #pragma omp parallel for
        for(int i=0; i < from_h; i++)
            for(int j=from_w - 1; j >= 0; j--)
                if(mask_threshold == -1 || mask[i][j] > mask_threshold)
                    HASH_FROM_CODES;
        break;

    case 2:
        #pragma omp parallel for
        for(int i=from_h - 1; i >= 0; i--)
            for(int j=0; j < from_w; j++)
                if(mask_threshold == -1 || mask[i][j] > mask_threshold)
                    HASH_FROM_CODES;
        break;

    case 3:
        #pragma omp parallel for
        for(int i=from_h - 1; i >= 0; i--)
            for(int j=from_w - 1; j >= 0; j--)
                if(mask_threshold == -1 || mask[i][j] > mask_threshold)
                    HASH_FROM_CODES;
        break;
    }

    free(hash_table[0]);
    free(hash_table[1]);
    free(bits_used);

    if(verbose) {
        printf("nb_new_matches: %d\n", nb_new_matches);
        printf("nb_better_matches: %d\n", nb_better_matches);
    }
}

int main(int argc, char** argv) {

    int nthreads = 4, max_iterations = 30,
        disable_heuristics = 0, proj_lut = 0, dump_all_images = 0;

    int max_heuristic_iterations = 5;

    int use_default_out_format = 1;

    int quadratic = 1;

    /*
      ~~~ TODO ~~~

      Stop the iterations after the condition is met :

      | delta(nb_better_matches + nb_new_matches) | < stop_threshold

      For stop_threshold_nb_passes iterations

     */
    // float stop_threshold = 0.1;
    // int stop_threshold_nb_passes = 3;

    char* ref_format = "leo_%03d.png";
    char* cam_format = "%03d.png";
    char* out_format = "lutCam%02d.png";

    char filename[FNAME_MAX_LEN]; // Generic filename buffer

    // Args parsing
    ARGBEGIN
    ARG_CASE('t')
        nthreads = ARGI;

    ARG_CASE('v')
        verbose = 1;

    ARG_CASE('p')
        proj_lut = 1;
        mask_threshold = -1;

    ARG_CASE('R')
        ref_format = ARGS;

    ARG_CASE('C')
        cam_format = ARGS;

    ARG_CASE('O')
        out_format = ARGS;
        use_default_out_format = 0;

    ARG_CASE('i')
        max_iterations = ARGI;

    ARG_CASE('d')
        disable_heuristics = 1;

    ARG_CASE('b')
        nb_selected_bits = ARGI;

    ARG_CASE('m')
        if(proj_lut) {
            fprintf(stderr, "*** WARNING : -m has no effect when -p is enabled\n");
        } else {
            mask_threshold = ARGF;
        }

    ARG_CASE('l')
        quadratic = 0;

    LARG_CASE("dump-all-images")
        dump_all_images = 1;

    WRONG_ARG
        printf("usage: %s [-t nb_threads=%d] [-v verbose] [-p projector LUT]\n"
               "\t[-R ref_format=\"%s\"] [-C cam_format=\"%s\"]\n"
               "\t[-O out_format=\"%s\"]\n"
               "\t[-i max_iterations=%d] [-d disable heuristics]\n"
               "\t[-b nb_selected_bits=%d] [-l (linear codes)]\n"
               "\t[-m mask_threshold=%0.2f] [--dump-all-images]\n\n",
               argv0, nthreads, ref_format, cam_format, out_format, max_iterations,
               nb_selected_bits, mask_threshold
            );
        exit(1);

    ARGEND

    omp_set_num_threads(nthreads);

    srand(time(NULL));

    // Check file size to avoid problems if patterns.txt is empty
    FILE* info = fopen("patterns.txt", "r");

    if(info != NULL)
        fseek(info, 0, SEEK_END);

    if(info == NULL || !ftell(info)) {
        printf("error: empty patterns.txt\n");
        exit(-1);
    }
    fseek(info, 0, SEEK_SET);

    fscanf(info, "%d %d %d",
           &to_w, &to_h, &nb_patterns);

    fclose(info);

    // Lecture d'une image pour trouver le from_w, from_h
    sprintf(filename, cam_format, 0);
    free_f32matrix(load_gray(filename, &from_w, &from_h));

    int nb_pixels_max = fmax(to_w * to_h, from_w * from_h);

    if(nb_selected_bits == -1) {
        nb_selected_bits = (int) ceil(log2f(nb_pixels_max));
    }

    // Generating projector LUT => read captured images as projected
    // images
    if(proj_lut) {
        char* tmp_format = ref_format;
        ref_format = cam_format;
        cam_format = tmp_format;

        if(use_default_out_format)
            out_format = "lutProj%02d.png";

        int tmp;
        tmp = from_w;
        from_w = to_w;
        to_w = tmp;

        tmp = from_h;
        from_h = to_h;
        to_h = tmp;
    }

    float*** matches = malloc_f32cube(3, from_w, from_h); // matches[ x, y, distance ][h=i][w=j]

    #pragma omp parallel for
    for(int k=0; k<3; k++)
        for(int i=0; i<from_h; i++)
            for(int j=0; j<from_w; j++)
                matches[k][i][j] = -1.0;

    nb_bits = nb_patterns - 1;

    mpz_t** cam_codes;
    mpz_t** ref_codes;

    if(quadratic) {
        nb_bits = nb_patterns * (nb_patterns - 1) / 2;

        if(verbose)
            printf("Loading cam binary codes\n");
        cam_codes = load_binary_codesq(cam_format, from_w, from_h, nb_patterns);

        if(verbose)
            printf("Loading ref binary codes\n");
        ref_codes = load_binary_codesq(ref_format, to_w, to_h, nb_patterns);
    } else {
        if(verbose)
            printf("Loading cam binary codes\n");
        cam_codes = load_binary_codesl(cam_format, from_w, from_h, nb_patterns);

        if(verbose)
            printf("Loading ref binary codes\n");
        ref_codes = load_binary_codesl(ref_format, to_w, to_h, nb_patterns);
    }

    if(mask_threshold != -1) {
        mask = load_mask(proj_lut ? ref_format : cam_format, nb_patterns, from_w, from_h);
    }

    time_since_start = time(NULL);

    for(int l=0; l<max_iterations; l++) {
        if(verbose)
            printf("----- Iteration %02d -----\n", l);
        lsh(matches, cam_codes, ref_codes);

        // Save the iteration
        if(dump_all_images) {
            sprintf(filename, out_format, l);

            save_color_map(filename, matches,
                           from_w, from_h, to_w, to_h, nb_bits);
        }

        if(verbose)
            printf("delta time = %ld\n", time(NULL) - time_since_start);
    }

    if(!disable_heuristics) {

        save_color_map(proj_lut ? "lutProj-no-heuristic.png" : "lutCam-no-heuristic.png",
                       matches, from_w, from_h, to_w, to_h, nb_bits);

        free_mpz_matrix(cam_codes);
        free_mpz_matrix(ref_codes);

        // Load linear continuous codes for the spatial propagation_cost heuristic
        float*** cont_from_codes = NULL;
        float*** cont_to_codes = NULL;

        cont_from_codes = malloc_f32cube(from_h, nb_patterns, from_w);
        cont_to_codes = malloc_f32cube(to_h, nb_patterns, to_w);

        if(verbose)
            printf("Loading cam continuous codes\n");
        load_intensities(cont_from_codes, cam_format, from_w, from_h, nb_patterns, 1);

        if(verbose)
            printf("Loading ref continuous codes\n");
        load_intensities(cont_to_codes, ref_format, to_w, to_h, nb_patterns, 1);

        // Normalize distances
        for(int i=0; i < from_h; i++)
            for(int j=0; j < from_w; j++) {
                char is_unmatched = matches[DIST][i][j] == -1.0;

                if(is_unmatched)
                    continue;

                int match_x = matches[X][i][j];
                int match_y = matches[Y][i][j];

                float cost = pixel_cost(
                    cont_from_codes[i][j],
                    cont_to_codes[match_y][match_x],
                    nb_patterns);

                matches[DIST][i][j] = cost;
            }

        for(int iteration=0; iteration<max_heuristic_iterations; iteration++) {
            int nb_better = spatial_propagation(matches, cont_from_codes, cont_to_codes);

            if(verbose)
                printf("delta time = %ld\n", time(NULL) - time_since_start);

            if(nb_better == 0)
                break;
        }
    }

    save_color_map(
        proj_lut ? "lutProjPixel.png" : "lutCamPixel.png",
        matches, from_w, from_h, to_w, to_h, 1.0);

    return EXIT_SUCCESS;
}
