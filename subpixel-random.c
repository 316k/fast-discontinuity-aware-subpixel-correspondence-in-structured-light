/*
  Compute a subpixel-accurate LUT from a pixel-accurate LUT
*/
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <stdint.h>

#include "args.h"
#include "helpers.h"


int nb_patterns, from_w, from_h, to_w, to_h, nb_bits;
int n_random_dimensions = 50;
int verbose = 0;

static inline float fmax4(float a, float b, float c, float d) {
    float ab = fmax(a, b);
    float cd = fmax(c, d);
    return fmax(ab, cd);
}

static inline float fmin4(float a, float b, float c, float d) {
    float ab = fmin(a, b);
    float cd = fmin(c, d);
    return fmin(ab, cd);
}

/**
 * Compute the subpixel cost between a camera code and four reference
 * pixels interpolated with (0, 0) <= (dx, dy) <= (1, 1)
 *
 * See Eq (9) of Subpixel Unsynchronized Unstructured Light, Chaima El
 * Asmi, Sébastien Roy
 */
float subpixel_cost(float dx, float dy,
                    float* cam_code,
                    float* a, float* b, float* c, float* d) {

    float dot_product = 0;
    float norm_cam = 0;
    float norm_ref = 0;

    for(int k=0; k<nb_bits; k++) {
        float interpolated_code = billy(dx, dy, a[k], b[k], c[k], d[k]);

        dot_product += interpolated_code * cam_code[k];
        norm_cam += SQUARE(cam_code[k]);
        norm_ref += SQUARE(interpolated_code);
    }

    norm_cam = sqrt(norm_cam);
    norm_ref = sqrt(norm_ref);

    return (1 - dot_product / (norm_ref * norm_cam)) / 2;
}

struct two_solutions {
    float dx1;
    float dy1;
    float dx2;
    float dy2;
};
typedef struct two_solutions two_solutions;

int find_intersections(
    float bi, float ci, float di, float si,
    float bj, float cj, float dj, float sj, two_solutions* sols) {

    float A1 = bj * (ci - di) + bi * (-cj + dj);
    float A2 = cj * (-bi + di) + ci * (bj - dj);

    if(A1 == 0 || A2 == 0)
        return 0;

    float B1 = -bj * ci + bi * cj;
    float B2 = si * (bj + cj - dj) + sj * (-bi - ci + di);
    float K = -cj * si + ci * sj;

    float delta = SQUARE(B1 + B2) - 4 * A1 * K;

    if(delta < 0)
        return 0;

    sols->dx1 = (-(B1 + B2) - sqrt(delta)) / (2 * A1);
    sols->dy1 = (-(B1 - B2) - sqrt(delta)) / (2 * A2);

    sols->dx2 = (-(B1 + B2) + sqrt(delta)) / (2 * A1);
    sols->dy2 = (-(B1 - B2) + sqrt(delta)) / (2 * A2);

    return 1;
}

int main(int argc, char** argv) {

    int nthreads = 4;
    int discontinuity_threshold = 1, keep_pixel_discontinuities = 1;
    int proj_lut = 0;

    char* ref_format = "leo_%03d.png";
    char* cam_format = "%03d.png";

    char* output_fname = "lutCamSubpixel.png";
    int orig_outputfname = 1;

    char* lut_fname = "lutCamPixel.png";
    char* lutproj_fname = "lutProjPixel.png";

    char* dump_prefix = "dump-subpix";

    char filename[FNAME_MAX_LEN]; // Generic filename buffer

    // Args parsing
    ARGBEGIN
        ARG_CASE('t')
        nthreads = ARGI;

    ARG_CASE('v')
        verbose = 1;

    ARG_CASE('p')
        proj_lut = 1;

    ARG_CASE('O')
        output_fname = ARGS;
        orig_outputfname = 0;

    ARG_CASE('L')
        lutproj_fname = lut_fname = ARGS;

    ARG_CASE('R')
        ref_format = ARGS;

    ARG_CASE('C')
        cam_format = ARGS;

    ARG_CASE('D')
        dump_prefix = ARGS;

    ARG_CASE('n')
        n_random_dimensions = ARGI;

    LARG_CASE("remove-pixel-discontinuities")
        keep_pixel_discontinuities = 0;

    WRONG_ARG
        printf("usage: %s [-t nb_threads=%d] [-v verbose] [-p projector]\n"
               "\t[-O output=%s] [-L lut=%s]\n"
               "\t[-R ref_format=\"%s\"] [-C cam_format=\"%s\"]\n"
               "\t[-D dump-prefix=\"%s\"]\n"
               "\t[-n number of dimensions to use=%d]\n"
               "\t[--remove-pixel-discontinuities]\n",
               argv0, nthreads, output_fname, lut_fname,
               ref_format, cam_format, dump_prefix, (int) n_random_dimensions);
        exit(1);

    ARGEND

    omp_set_num_threads(nthreads);

    srand(time(NULL));

    // Check file size to avoid problems if patterns.txt is empty
    FILE* info = fopen("patterns.txt", "r");

    if(info != NULL)
        fseek(info, 0, SEEK_END);

    if(info == NULL || !ftell(info)) {
        printf("error: empty patterns.txt\n");
        exit(-1);
    }
    fseek(info, 0, SEEK_SET);

    fscanf(info, "%d %d %d %d",
           &to_w, &to_h, &nb_patterns, &discontinuity_threshold);

    fclose(info);

    // Read an image to get from_w & from_h
    sprintf(filename, cam_format, 0);
    free_f32matrix(load_gray(filename, &from_w, &from_h));

    nb_bits = nb_patterns;

    float*** cam_codes;
    float*** ref_codes;

    cam_codes = malloc_f32cube(from_h, nb_bits, from_w);
    ref_codes = malloc_f32cube(to_h, nb_bits, to_w);

    // Camera *float* codes
    if(verbose)
        printf("Loading cam codes\n");
    load_intensities(cam_codes, cam_format, from_w, from_h, nb_patterns, 0);

    // Ref *float* codes
    if(verbose)
        printf("Loading ref codes\n");
    load_intensities(ref_codes, ref_format, to_w, to_h, nb_patterns, 0);

    // Load pixel-precise LUT
    float*** matches = load_color(lut_fname, &from_w, &from_h);

    #pragma omp parallel for
    for(int i=0; i<from_h; i++)
        for(int j=0; j<from_w; j++) {

            if(matches[X][i][j] == 65535.0 && matches[Y][i][j] == 65535.0 && matches[DIST][i][j] == 65535.0) {
                matches[X][i][j] = matches[Y][i][j] = matches[DIST][i][j] = -1.0;
            } else {
                matches[X][i][j] = round(matches[X][i][j] / 65535.0 * (to_w - 1));
                matches[Y][i][j] = round(matches[Y][i][j] / 65535.0 * (to_h - 1));
                matches[DIST][i][j] = matches[DIST][i][j] / 65535.0;
            }
        }

    normalize_codes(cam_codes, from_w, from_h, nb_patterns);
    normalize_codes(ref_codes, to_w, to_h, nb_patterns);

    if(proj_lut) {

        int tmp;
        tmp = from_w;
        from_w = to_w;
        to_w = tmp;

        tmp = from_h;
        from_h = to_h;
        to_h = tmp;

        // Swap cam/ref
        float*** tmp_codes = ref_codes;
        ref_codes = cam_codes;
        cam_codes = tmp_codes;

        // Reload w/ proj matches
        free_f32cube(matches, 3);
        matches = load_color(lutproj_fname, &from_w, &from_h);

        #pragma omp parallel for
        for(int i=0; i<from_h; i++)
            for(int j=0; j<from_w; j++) {

                if(matches[DIST][i][j] == 65535.0) {
                    matches[X][i][j] = matches[Y][i][j] = matches[DIST][i][j] = -1.0;
                } else {
                    matches[X][i][j] = round(matches[X][i][j] / 65535.0 * (to_w - 1));
                    matches[Y][i][j] = round(matches[Y][i][j] / 65535.0 * (to_h - 1));
                    matches[DIST][i][j] = matches[DIST][i][j] / 65535.0;
                }
            }

        if(orig_outputfname) {
            output_fname = "lutProjSubpixel.png";
        }
    }

    float*** subpix_matches = malloc_f32cube(3, from_w, from_h);
    float*** discontinuity_map = malloc_f32cube(3, from_w, from_h);

    if(verbose)
        printf("Computing subpixel LUT...\n");

    int progress_bar_increment = from_h / 50;

    if(verbose && progress_bar_increment) {
        // Progress-bar
        for(int i=0; i<from_h; i += progress_bar_increment) {
            fprintf(stderr, ".");
        }
        fprintf(stderr, "\n");
    }

    // Cached randomish values -- use with random[(rnd += 1) % nrand]
    const int nrand = 8192;
    int random[nrand];
    int prev = -1;

    for(int i=0; i<nrand; i++) {
        int val;
        do {
            val = (int) (rand() / (float)RAND_MAX * nb_bits);
        } while(val == nb_bits || val == prev || (i == nrand - 1 && val == random[0]));
        random[i] = val;
        prev = val;
    }

    long last = time(NULL);
    int total_ops = 0;

    #pragma omp parallel for
    for(int i=0; i<from_h; i++) {

        int rnd = rand() % nrand;

        if(verbose && progress_bar_increment && i % progress_bar_increment == 0)
            fprintf(stderr, ".");

        for(int j=0; j<from_w; j++) {

            int match_x = (int) matches[X][i][j];
            int match_y = (int) matches[Y][i][j];
            float cost = matches[DIST][i][j];

            // Skip borders & unmatched pixels
            if(cost == -1 ||
               i == 0 || i == from_h - 1 ||
               j == 0 || j == from_w - 1 ||
               match_x == 0 || match_x >= to_w - 1 ||
               match_y == 0 || match_y >= to_h - 1) {
                subpix_matches[X][i][j] = match_x;
                subpix_matches[Y][i][j] = match_y;
                subpix_matches[DIST][i][j] = cost;
                continue;
            }

            float best_subpix_x = match_x;
            float best_subpix_y = match_y;
            int is_best_discontinuity = 0;
            int best_quadrant = -1;
            float newcost = INFINITY;

            size_t nb_solutions = 0;

            // quadrant =      0           1         2            3
            //=> (x,y) = (   -1,-1   ;   +1,-1  ;   -1,+1   ;   +1,+1   )
            //              red  '\     pink /' ; yellow _/ ; white \_
            for(int quadrant=0; quadrant<4; quadrant++) {

                int current_x = j, current_y = i;
                int current_match_x = match_x, current_match_y = match_y;

                int delta_x = (quadrant == 0 || quadrant == 2) ? -1 : +1;
                int delta_y = quadrant < 2 ? -1 : +1;

                int neighbour_unmatched =
                    matches[DIST][i][j] == -1 ||
                    matches[DIST][i][j + delta_x] == -1 ||
                    matches[DIST][i + delta_y][j] == -1 ||
                    matches[DIST][i + delta_y][j + delta_x] == -1;

                if(neighbour_unmatched) {
                    continue;
                }

                // Is this quadrant representing a discontinuity?
                int match_ax = matches[X][i][j],
                    match_bx = matches[X][i][j + delta_x],
                    match_cx = matches[X][i + delta_y][j],
                    match_dx = matches[X][i + delta_y][j + delta_x],
                    match_ay = matches[Y][i][j],
                    match_by = matches[Y][i][j + delta_x],
                    match_cy = matches[Y][i + delta_y][j],
                    match_dy = matches[Y][i + delta_y][j + delta_x];

                int max_distance_x = fmax4(match_ax, match_bx, match_cx, match_dx)
                    - fmin4(match_ax, match_bx, match_cx, match_dx);
                int max_distance_y = fmax4(match_ay, match_by, match_cy, match_dy)
                    - fmin4(match_ay, match_by, match_cy, match_dy);

                int is_discontinuity =
                    max_distance_x > discontinuity_threshold ||
                    max_distance_y > discontinuity_threshold;
                
                if(!is_discontinuity) {
                    // Adjust "current pixel" to always use top-left pixel
                    if(quadrant < 2)
                        current_match_y--;

                    if(quadrant % 2 == 0)
                        current_match_x--;
                }

                assert(current_match_x >= 0);
                assert(current_match_y >= 0);
                assert(current_match_x < to_w - 1);
                assert(current_match_y < to_h - 1);

                float *ref_a, *ref_b, *ref_c, *ref_d;

                if(is_discontinuity) {
                    // Interpolate between non-neighboring pixels
                    ref_a = ref_codes[match_ay][match_ax];
                    ref_b = ref_codes[match_by][match_bx];
                    ref_c = ref_codes[match_cy][match_cx];
                    ref_d = ref_codes[match_dy][match_dx];
                } else {
                    // Interpolate between adjacent pixels
                    ref_a = ref_codes[current_match_y][current_match_x];
                    ref_b = ref_codes[current_match_y][current_match_x + 1];
                    ref_c = ref_codes[current_match_y + 1][current_match_x];
                    ref_d = ref_codes[current_match_y + 1][current_match_x + 1];
                }

                for(int n=0; n<n_random_dimensions; n++) {
                    total_ops++;
                    int dim_a = random[(rnd += 1) % nrand];
                    int dim_b = random[(rnd += 1) % nrand];

                    assert(dim_a != dim_b);

                    assert(dim_a < nb_bits);
                    assert(dim_b < nb_bits);

                    // Construct the i^th bilinear surface
                    float zi = cam_codes[current_y][current_x][dim_a];
                    float ai = ref_a[dim_a];
                    float bi = ref_b[dim_a];
                    float ci = ref_c[dim_a];
                    float di = ref_d[dim_a];

                    if(zi > fmax4(ai, bi, ci, di) || zi < fmin4(ai, bi, ci, di)) {
                        // The i^th bilinear surface does not
                        // intersect the captured intensity
                        continue;
                    }

                    // Idem with the j^th surface
                    float zj = cam_codes[current_y][current_x][dim_b];
                    float aj = ref_a[dim_b];
                    float bj = ref_b[dim_b];
                    float cj = ref_c[dim_b];
                    float dj = ref_d[dim_b];

                    if(zj > fmax4(aj, bj, cj, dj) || zj < fmin4(aj, bj, cj, dj)) {
                        continue;
                    }

                    // Quadratic curves intersections
                    two_solutions sols;

                    if(find_intersections(bi - ai, ci - ai, di - ai, zi - ai,
                                          bj - aj, cj - aj, dj - aj, zj - aj, &sols)) {

                        // First solution is valid
                        if(fmin(sols.dx1, sols.dy1) >= 0 && fmax(sols.dx1, sols.dy1) <= 1) {
                            // solutions_x[nb_solutions * 2 + 0] = current_match_x + sols.dx1;
                            // solutions_y[nb_solutions * 2 + 1] = current_match_y + sols.dy1;
                            nb_solutions++;

                            float cost = subpixel_cost(sols.dx1, sols.dy1,
                                                       cam_codes[current_y][current_x],
                                                       ref_a, ref_b, ref_c, ref_d);

                            if(cost < newcost) {
                                // Better solution found in a smooth quadrant
                                best_subpix_x = current_match_x + sols.dx1;
                                best_subpix_y = current_match_y + sols.dy1;
                                is_best_discontinuity = is_discontinuity;
                                best_quadrant = quadrant;
                                newcost = cost;
                            }
                        }

                        // Second solution is valid
                        if(fmin(sols.dx2, sols.dy2) >= 0 && fmax(sols.dx2, sols.dy2) <= 1) {
                            // solutions_x[nb_solutions * 2 + 0] = current_match_x + sols.dx2;
                            // solutions_y[nb_solutions * 2 + 1] = current_match_y + sols.dy2;
                            nb_solutions++;

                            float cost = subpixel_cost(sols.dx2, sols.dy2,
                                                       cam_codes[current_y][current_x],
                                                       ref_a, ref_b, ref_c, ref_d);

                            if(cost < newcost) {
                                // Better solution found in a smooth quadrant
                                best_subpix_x = current_match_x + sols.dx2;
                                best_subpix_y = current_match_y + sols.dy2;
                                is_best_discontinuity = is_discontinuity;
                                best_quadrant = quadrant;
                                newcost = cost;
                            }
                        }
                    }
                }
            }

            int use_pixel_match = newcost == INFINITY ||
                (is_best_discontinuity && keep_pixel_discontinuities);

            if(use_pixel_match) {
                // Keep pixel-accurate match
                subpix_matches[X][i][j] = match_x;
                subpix_matches[Y][i][j] = match_y;
            } else if(is_best_discontinuity) {
                // Discard pixel entirely
                subpix_matches[X][i][j] = -1;
                subpix_matches[Y][i][j] = -1;
                subpix_matches[DIST][i][j] = -1;
            } else {
                subpix_matches[X][i][j] = best_subpix_x;
                subpix_matches[Y][i][j] = best_subpix_y;
                subpix_matches[DIST][i][j] = newcost;
            }

            discontinuity_map[X][i][j] = is_best_discontinuity ? 255 : 0;
            discontinuity_map[Y][i][j] = (is_best_discontinuity && (best_quadrant & 2)) ? 255 : 0;
            discontinuity_map[DIST][i][j] = (is_best_discontinuity && (best_quadrant & 1)) ? 255 : 0;
        }
    }

    if(verbose) {
        putchar('\n');
        printf("time: %ld\n", time(NULL) - last);
        printf("N=%d\n", total_ops);
    }

    sprintf(filename, "%s-x.dat", dump_prefix);
    FILE *fdebug = fopen(filename, "w");

    for(int i=0; i<from_h; i++) {
        for(int j=0; j<from_w; j++) {
            fprintf(fdebug, "%f ", subpix_matches[X][i][j]);
        }

        putc('\n', fdebug);
    }

    fclose(fdebug);

    sprintf(filename, "%s-y.dat", dump_prefix);
    fdebug = fopen(filename, "w");

    for(int i=0; i<from_h; i++) {
        for(int j=0; j<from_w; j++) {
            fprintf(fdebug, "%f ", subpix_matches[Y][i][j]);
        }

        putc('\n', fdebug);
    }

    fclose(fdebug);

    save_color_png("discontinuities.png", discontinuity_map, from_w, from_h, 8);

    // Write subpixel-precise LUT
    save_color_map(
        output_fname,
        subpix_matches, from_w, from_h, to_w, to_h, 1.0);

    return EXIT_SUCCESS;
}
