/*
  Compute a subpixel-accurate LUT from a pixel-accurate LUT using a
  gradient descent
*/
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "args.h"
#include "helpers.h"


int nb_patterns, from_w, from_h, to_w, to_h, nb_bits;

/**
 * Compute the subpixel cost between a camera code and four reference
 * pixels interpolated with (0, 0) <= (dx, dy) <= (1, 1)
 *
 * See Eq (9) of Subpixel Unsynchronized Unstructured Light, Chaima El
 * Asmi, Sébastien Roy
 */
float subpixel_cost(float dx, float dy,
                    float* cam_code,
                    float* a, float* b, float* c, float* d) {

    float dot_product = 0;
    float norm_cam = 0;
    float norm_ref = 0;

    for(int k=0; k<nb_bits; k++) {
        float interpolated_code = billy(dx, dy, a[k], b[k], c[k], d[k]);

        dot_product += interpolated_code * cam_code[k];
        norm_cam += SQUARE(cam_code[k]);
        norm_ref += SQUARE(interpolated_code);
    }

    norm_cam = sqrt(norm_cam);
    norm_ref = sqrt(norm_ref);

    return (1 - dot_product / (norm_ref * norm_cam)) / 2;
}

float coord_subpixel_cost(float x, float y, float* m, float*** ref_codes) {

    int integer_x = (int) x;
    int integer_y = (int) y;

    float* ref_a = ref_codes[integer_y][integer_x];
    float* ref_b = ref_codes[integer_y][integer_x + 1];
    float* ref_c = ref_codes[integer_y + 1][integer_x];
    float* ref_d = ref_codes[integer_y + 1][integer_x + 1];

    return subpixel_cost(x - integer_x, y - integer_y, m, ref_a, ref_b, ref_c, ref_d);
}

// Gradient descent parameters
const float step_decrease_rate = 0.9999;
int max_iters = 1000;

// Gradient descent constants
const float epsilon = 0.01;
float precision = 1e-6;

int biggest_iter_reached = 0, max_iters_reached = 0, nb_gradient_descents = 0;

/**
 * Find subpixel through a gradient descent
 */
int gradient_descent_solution(float* m, float*** ref_codes,
                              float* match_x, float* match_y) {
    nb_gradient_descents++;

    float previous_step_size = INFINITY;

    int iter = 0;
    float step = 1;

    float x = *match_x;
    float y = *match_y;

    for(iter=0; previous_step_size > precision && iter < max_iters; iter++) {

        float current_cost = coord_subpixel_cost(x, y, m, ref_codes);
        float grad_x = (coord_subpixel_cost(x + epsilon, y, m, ref_codes) - current_cost) / epsilon;
        float grad_y = (coord_subpixel_cost(x, y + epsilon, m, ref_codes) - current_cost) / epsilon;

        float prev_x = x, prev_y = y;

        // Decrease speed over time to prevent loops or going too far
        // away from starting point
        step *= step_decrease_rate;

        float delta_x = step * fmax(fmin(grad_x, 0.1), -0.1);
        float delta_y = step * fmax(fmin(grad_y, 0.1), -0.1);

        x -= delta_x;
        y -= delta_y;

        x = fmax(fmin(x, to_w - 2), 1);
        y = fmax(fmin(y, to_h - 2), 1);

        previous_step_size = fmax(fabs(prev_x - x), fabs(prev_y - y));
    }

    biggest_iter_reached = fmax(biggest_iter_reached, iter);

    if(iter == max_iters) {
        max_iters_reached++;
    }

    *match_x = x;
    *match_y = y;

    return 1;
}

int main(int argc, char** argv) {

    int nthreads = 4;
    int verbose = 0;

    char* ref_format = "leo_%03d.png";
    char* cam_format = "%03d.png";

    char* lut_fname = "lutCamPixel.png";
    char* output_fname = "lutCamSubpixel-gradient.png";

    char* dump_prefix = "dump-grad";

    char filename[FNAME_MAX_LEN]; // Generic filename buffer

    // Args parsing
    ARGBEGIN
        ARG_CASE('t')
        nthreads = ARGI;

    ARG_CASE('v')
        verbose = 1;

    ARG_CASE('i')
        max_iters = ARGI;

    LARG_CASE("precision")
        precision = powf(10, -ARGI);

    ARG_CASE('R')
        ref_format = ARGS;

    ARG_CASE('C')
        cam_format = ARGS;

    ARG_CASE('D')
        dump_prefix = ARGS;

    ARG_CASE('L')
        lut_fname = ARGS;

    ARG_CASE('O')
        output_fname = ARGS;

    WRONG_ARG
        printf("usage: %s [-t nb_threads=%d] [-v verbose]\n"
               "\t[-D dump-prefix=\"%s\"]\n"
               "\t[-R ref_format=\"%s\"] [-C cam_format=\"%s\"]\n"
               "\t[-O output=%s] [-L lut=%s]\n"
               "\t[-i max iterations=%d] [--precision N]\n"
               "\n"
               "\tPrecision=N tries to reach a point where delta_val < 10**N\n",
               argv0, nthreads, dump_prefix,
               ref_format, cam_format,
               output_fname, lut_fname,
               max_iters);
        exit(1);

    ARGEND

    omp_set_num_threads(nthreads);

    srand(time(NULL));

    // Check file size to avoid problems if patterns.txt is empty
    FILE* info = fopen("patterns.txt", "r");

    if(info != NULL)
        fseek(info, 0, SEEK_END);

    if(info == NULL || !ftell(info)) {
        printf("error: empty patterns.txt\n");
        exit(-1);
    }
    fseek(info, 0, SEEK_SET);

    fscanf(info, "%d %d %d",
           &to_w, &to_h, &nb_patterns);

    fclose(info);

    // Read an image to get from_w & from_h
    sprintf(filename, cam_format, 0);
    free_f32matrix(load_gray(filename, &from_w, &from_h));


    nb_bits = nb_patterns;

    float*** cam_codes;
    float*** ref_codes;

    cam_codes = malloc_f32cube(from_h, nb_bits, from_w);
    ref_codes = malloc_f32cube(to_h, nb_bits, to_w);

    // Camera *float* codes
    if(verbose)
       printf("Loading cam codes\n");
    load_intensities(cam_codes, cam_format, from_w, from_h, nb_patterns, 1);

    // Ref *float* codes
    if(verbose)
        printf("Loading ref codes\n");
    load_intensities(ref_codes, ref_format, to_w, to_h, nb_patterns, 1);

    // Load pixel-precise LUT
    float*** matches = load_color(lut_fname, &from_w, &from_h);

    #pragma omp parallel for
    for(int i=0; i<from_h; i++)
        for(int j=0; j<from_w; j++) {

            if(matches[X][i][j] == 65535.0) {
                matches[X][i][j] = matches[Y][i][j] = matches[DIST][i][j] = -1.0;
            } else {
                matches[X][i][j] = round(matches[X][i][j] / 65535.0 * (to_w - 1));
                matches[Y][i][j] = round(matches[Y][i][j] / 65535.0 * (to_h - 1));
                matches[DIST][i][j] = M_PI;
            }
        }

    float*** subpix_matches = malloc_f32cube(3, from_w, from_h);

    // Fill with pixel matches
    #pragma omp parallel for
    for(int i=0; i<from_h; i++) {
        for(int j=0; j<from_w; j++) {
            subpix_matches[X][i][j] = matches[X][i][j];
            subpix_matches[Y][i][j] = matches[Y][i][j];
            subpix_matches[DIST][i][j] = matches[DIST][i][j];
        }
    }

    if(verbose)
        printf("Computing subpixel LUT...\n");

    int progress_bar_increment = from_h / 50;

    if(verbose && progress_bar_increment) {
        // Progress-bar
        for(int i=0; i<from_h; i += progress_bar_increment) {
            fprintf(stderr, ".");
        }
        fprintf(stderr, "\n");
        fprintf(stderr, "Total: %d\n", progress_bar_increment);
    }

    long last = time(NULL);

    #pragma omp parallel for
    for(int i=0; i<from_h; i++) {

        if(verbose && progress_bar_increment && i % progress_bar_increment == 0) {
            fprintf(stderr, ".");
            fprintf(stderr, "Max iter reached: %f %d/%d (%4.2f)\n", nb_gradient_descents / (from_w * from_h * 4.0), max_iters_reached, nb_gradient_descents, max_iters_reached/(float) nb_gradient_descents);
        }

        for(int j=0; j<from_w; j++) {

            int match_x = (int) matches[X][i][j];
            int match_y = (int) matches[Y][i][j];
            float cost = matches[DIST][i][j];

            // Skip subpixel match on image borders
            if(cost == -1 ||
               i == 0 || i == from_h - 1 ||
               j == 0 || j == from_w - 1 ||
               // Border
               match_x == 0 || match_x >= to_w - 1 ||
               match_y == 0 || match_y >= to_h - 1) {
                subpix_matches[X][i][j] = match_x;
                subpix_matches[Y][i][j] = match_y;
                subpix_matches[DIST][i][j] = 1;
                continue;
            }

            float new_x = match_x, new_y = match_y;

            float* m = cam_codes[i][j];

            gradient_descent_solution(m, ref_codes, &new_x, &new_y);

            float* a = ref_codes[(int) new_y][(int) new_x];
            float* b = ref_codes[(int) new_y][(int) new_x + 1];
            float* c = ref_codes[(int) new_y + 1][(int) new_x];
            float* d = ref_codes[(int) new_y + 1][(int) new_x + 1];

            subpix_matches[X][i][j] = new_x;
            subpix_matches[Y][i][j] = new_y;
            subpix_matches[DIST][i][j] = subpixel_cost(new_x - ((int) new_x), new_y - ((int) new_y), m, a, b, c, d);
        }
    }

    if(verbose) {
        putchar('\n');
        printf("time: %ld\n", time(NULL) - last);
    }

    sprintf(filename, "%s-x.dat", dump_prefix);
    FILE *fdebug = fopen(filename, "w");

    for(int i=0; i<from_h; i++) {
        for(int j=0; j<from_w; j++) {
            fprintf(fdebug, "%f ", subpix_matches[X][i][j]);
        }

        putc('\n', fdebug);
    }

    fclose(fdebug);

    sprintf(filename, "%s-y.dat", dump_prefix);
    fdebug = fopen(filename, "w");

    for(int i=0; i<from_h; i++) {
        for(int j=0; j<from_w; j++) {
            fprintf(fdebug, "%f ", subpix_matches[Y][i][j]);
        }

        putc('\n', fdebug);
    }

    fclose(fdebug);

    if(verbose) {
        printf("Gradient descent:\n"
               "\tbiggest iteration reached: %d\n"
               "\tmax iteration reached %d times\n"
               "\t(%d gradient descents total)\n",
               biggest_iter_reached, max_iters_reached, nb_gradient_descents);
    }

    // Write subpixel-precise LUT
    save_color_map(
        output_fname,
        subpix_matches, from_w, from_h, to_w, to_h, M_PI);

    return EXIT_SUCCESS;
}
