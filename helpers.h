
#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>

#include <math.h>

#include "lodepng.h"

extern int errno;

#define X 0
#define Y 1
#define DIST 2

#define FNAME_MAX_LEN 255

#define SQUARE(a) ((a) * (a))

#define M_PI acos(-1.0)

void require_file(FILE* f, char* fname);

// --------------- Images ---------------
float** malloc_f32matrix(int w, int h);

void free_f32matrix(float** image);

uint16_t** malloc_uint16matrix(int dim2, int dim1);

void free_uint16matrix(uint16_t** image);

// TODO : (w, h, k)  is a bad convention
float*** malloc_f32cube(int dim1, int dim3, int dim2);

void free_f32cube(float*** cube, int k);

void read_image_header(FILE* f, int *w, int *h, int *size);

float** load_pgm(char* name, int *w, int *h);

float*** load_ppm(char* name, int *w, int *h);

void save_pgm(char* filename, float** image, int w, int h, int depth);

void save_ppm(char* filename, float** channels[3], int w, int h, int depth);

unsigned char* load_png(char* fname, int *w, int *h, int *depth, LodePNGColorType expected_type);

float** load_png_gray(char* fname, int *w, int *h);
float*** load_png_rgb(char* fname, int *w, int *h);

char is_png(char* fname);

float*** load_color(char* fname, int *w, int *h);
float** load_gray(char* fname, int *w, int *h);
void save_raw_png(char* filename, unsigned char* image, int w, int h, int depth, LodePNGColorType colortype);
void save_gray_png(char* filename, float** image, int w, int h, int depth);
void save_color_png(char* filename, float*** image, int w, int h, int depth);

// --------------- Misc calculations ---------------

int* random_numbers(int size, int total);

float solve_phase_term(float* intensities, int nb_shifts, float (*trigo)(float));

float solve_phase(float* intensities, int nb_shifts);

float billy(float u, float v, float a, float b, float c, float d);
float pixel_cost(float* cam_code, float* ref_code, int nb_bits);

float code_avg(float* code, int nb_bits);
float code_norm(float* code, int nb_bits);
void normalize_codes(float*** codes, int w, int h, int nb_bits);
float*** load_continuous_codesq(char* format, int width, int height, int nb_patterns);

void load_continuous_codes(float*** codes, char* format,
                           int width, int height, int nb_patterns,
                           int quadratic);

void load_intensities(float*** codes, char* format,
                      int width, int height, int nb_patterns, int normalize);

float** load_mask(char* img_format, int nb_patterns, int w, int h);

/**
 * Remap an array of x/y/error to an array of 16-bit integers and save it
 */
void save_color_map(char* filename, float*** matches, int from_w, int from_h,
                   int to_w, int to_h, float max_distance);

