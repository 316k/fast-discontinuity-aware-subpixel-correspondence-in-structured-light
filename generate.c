/*
  Generates leopard patterns to be projected during the scan
*/
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "args.h"
#include "helpers.h"

int main(int argc, char** argv) {

    int nthreads = 4, w = 1920, h = 1080,
        nb_waves = 32, nb_patterns = 40,
        init_seed = 1337, verbose = 0;

    float base_freq = 0.05;

    int user_defined_band = 0;
    float freq_min, freq_max;

    float gray_min = 0, gray_max = 256;

    ARGBEGIN
    ARG_CASE('t')
    nthreads = ARGI;

    ARG_CASE('w')
        w = ARGI;

    ARG_CASE('h')
        h = ARGI;

    ARG_CASE('q')
        nb_waves = ARGI;

    ARG_CASE('n')
        nb_patterns = ARGI;

    ARG_CASE('f')
        base_freq = ARGF;

    ARG_CASE('v')
        verbose = 1;

    LARG_CASE("seed")
        init_seed = ARGI;

    LARG_CASE("custom-band")
        user_defined_band = 1;
        freq_min = ARGF;
        freq_max = ARGF;

    LSARG_CASE('r', "gray-range")
        gray_min = ARGF;
        gray_max = ARGF;

    WRONG_ARG
        printf("usage: %s [-t nb_threads=%d] [-v verbose]\n"
               "\t[-w width=%d] [-h height=%d]\n"
               "\t[-q nb_waves=%d] [-n nb_patterns=%d]\n"
               "\t[-f pattern_freq=%f] [--seed init_seed=%d]\n"
               "\t[--custom-band min max]\n",
               argv0, nthreads, w, h, nb_waves, nb_patterns,
               base_freq, init_seed);
    exit(0);

    ARGEND
        omp_set_num_threads(nthreads);

    float highest_freq = user_defined_band ? freq_max : base_freq;

    FILE* info = fopen("patterns.txt", "w+");
    fprintf(info, "%d %d %d %d\n", w, h, nb_patterns, (int) (1/highest_freq + 0.5));
    fclose(info);

    srand(init_seed);

    float* phases = malloc(sizeof(float) * nb_waves * nb_patterns);
    float* angles = malloc(sizeof(float) * nb_waves * nb_patterns);
    float* freqs  = malloc(sizeof(float) * nb_waves * nb_patterns);
    
    for(int n=0; n < nb_patterns; n++) {

        if(verbose)
            printf("Rendering pattern %03d\n", n);

        // Random waves
        for(int i=0; i<nb_waves; i++) {
            phases[n * nb_waves + i] = rand()/(float)RAND_MAX * 2 * M_PI;
            angles[n * nb_waves + i] = rand()/(float)RAND_MAX * M_PI;

            if(user_defined_band) {
                freqs[n * nb_waves + i] = rand()/(float)RAND_MAX * (freq_max - freq_min) + freq_min;
            } else {
                freqs[n * nb_waves + i] = base_freq;
            }
        }
    }


    #pragma omp parallel for
    for(int n=0; n < nb_patterns; n++) {

        // Init image matrix
        float** image = malloc_f32matrix(w, h);

        double total = 0;
        memset(image[0], 0, sizeof(float) * h * w);

        // Cos addition
        for(int wave = 0; wave < nb_waves; wave++) {

            float fx, fy, phase;

            fx = 2 * M_PI * freqs[n * nb_waves + wave] * sinf(angles[n * nb_waves + wave]);
            fy = 2 * M_PI * freqs[n * nb_waves + wave] * cosf(angles[n * nb_waves + wave]);
            phase = phases[n * nb_waves + wave];

            for(int i=0; i < h; i++) {
                for(int j=0; j < w; j++) {
                    float c = cosf(fy * i + fx * j + phase);
                    image[i][j] += c;
                    total += c;
                }
            }
        }

        // Adjust the pattern to get a flat histogram
        float mean = total / (w * h);

        double stddev = 0;
        for(int i=0; i < h; i++) {
            for(int j=0; j < w; j++) {
                double diff = image[i][j] - mean;
                stddev += SQUARE(diff);
            }
        }

        stddev = sqrt(stddev / (w * h - 1));
        float stretch = 3 * stddev;

        for(int i=0; i < h; i++) {
            for(int j=0; j < w; j++) {

                float adjusted_gray = erfc(4 * ((image[i][j] + stretch) / (2 * stretch) - 0.5)) / 2;
                image[i][j] = adjusted_gray * (gray_max - gray_min) + gray_min;

            }
        }

        char filename[FNAME_MAX_LEN];

        sprintf(filename, "leo_%03d.png", n);

        save_gray_png(filename, image, w, h, 8);

        free_f32matrix(image);
    }

    free(phases);
    free(angles);
    free(freqs);

    return EXIT_SUCCESS;
}
