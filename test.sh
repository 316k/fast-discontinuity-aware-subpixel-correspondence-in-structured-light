#!/bin/bash

make clean all

mkdir -p test
cd test

echo "Generating pattern"
../generate -w 300 -h 300 -n 20

echo "Capturing a synthetic subpixel"
i=0
for f in leo_*
do
    ../translation -x 0.35 -y -0.1 $f $(printf "%03d.png" $i)
    let i++
done

echo "Solving integer match"
../solve
../solve -p

echo "Subpixel refinement"
../subpixel-random -n 50
../subpixel-random -n 50 -p


echo "Subpixel refinement (gradient)"
../subpixel-gradient

echo "See test directory for results"
