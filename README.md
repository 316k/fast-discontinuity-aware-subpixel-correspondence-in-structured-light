Fast Discontinuity-Aware Subpixel Correspondence in Structured Light
====================================================================

This folder contains all the code necessary to run the Robust
Unstructured Light pixel-accurate algorithm [leo], the [grad] subpixel
refinement method and our Fast Discontinuity-Aware subpixel refinement
method.

## Build

This code has been tested with gcc and requires OpenMP and GMP
support.

Build the code with:

```bash
$ make
```

Windows users can use gcc under cygwin.

## Usage

1. Generate patterns
2. Capture them
3. Solve the correspondence with pixel-accuracy
4. Apply subpixel refinement for the pixel-accurate LUT


Generate patterns with:

```bash
$ ./generate
```

The default pattern frequency is 0.05 (1/20px) but should be increased
when possible.

```bash
$ ./generate -f 0.1  # 1/10px
```


Solve the pixel-accurate match with:

```bash
$ ./solve
```

By default, this will compute a camera -> projector lookup table. The
projector -> camera correspondence can be computed with the `-p`
option:

```bash
$ ./solve -p
```

The default path for reference patterns is `./leo_%03d.png`, the
default path for captured patterns is `./%03d.png`, but these can be
changed with the respective options `-R ...` and `-C ...`.

The [leo] algorithm is based on Local Sensitive Hashing, which is a
Monte Carlo algorithm. The more you give it time, the best the results
you'll get.

You can increase the number of iterations with the `-i` option, for
instance:

```bash
$ ./solve -i 100
$ ./solve -i 1000
```

You should however note that you will get diminishing returns, 1000
iterations is probably already way too much in most situations. The
best way to increase the accuracy is increase the number of captured
patterns with the `-n` option of `generate`.

A simple threshold is used to determine which pixels are part of the
background in the camera image. The threshold value can be changed
with the option `-m ...` using a value in [0, 255], or -1 to disable
it.

By default, `solve` will use the quadratic code (as defined in
[quad]), but this can be disabled if necessary with `-l` (e.g. if you
want to use a lot of patterns, which could make the quadratic code
bust your RAM).


Once the pixel-accurate LUT is obtained `lutCamPixel.png`, you can
refine it with `subpixel-random` (our method) or `subpixel-gradient`
(the global method defined in [grad]).

For our method, the number of random pairs can be selected with the
`-n` option:

```bash
$ ./subpixel-random -n 50
```

This is also a Monte Carlo algorithm, so increasing the `-n` parameter
will take more time but generate more accurate results. You should
note that the maximum number of pairs is

    N * (N - 1) / 2

Where N is the number of captured patterns. You should consider this
before selecting the `-n` value. For instance, with N=20, using `-n` >
190 would be somewhat absurd, as it would probably test multiple times
all possibles pairs.

As with the `solve` program, `subpixel-random` can compute a subpixel
refinement for the projector with `-p` (as long as `solve -p` was
previously run).


The gradient descent can be used instead of `subpixel-random` with:

```bash
$ ./subpixel-gradient
```

Full lists of options are available with the `--help` argument for all
programs.


A full synthetic example is provided in `test.sh`, which will build,
then place all patterns and results in the test directory:

```bash
$ ./test.sh
```

## Credit

If this code is useful to you, please cite our paper with:

```bibtex
@InProceedings{Hurtubise_2020_3DV,
    author = {Hurtubise, Nicolas and Roy, Sébastien},
    title = {Fast Discontinuity-Aware Subpixel Correspondence in Structured Light},
    booktitle = {2020 International Virtual Conference on 3D Vision (3DV)},
    month = {November},
    pages = {1108-1116},
    year = {2020}
}
```


## References

[leo]: V. Couture, N. Martin, and S. Roy. Unstructured light scanning robust to indirect illumination and depth discontinuities.  International Journal of Computer Vision, 108(3):204–221, July 2014.

[quad]: N. Martin, V. Chapdelaine-Couture, and S. Roy. Subpixel scanning invariant to indirect lighting using quadratic code length. pages 1441–1448, 12 2013.

[grad]: C. El Asmi and S. Roy. Subpixel unsynchronized unstructured light. pages 865–875, 01 2019.
