#include <unistd.h>
#include <errno.h>
#include <assert.h>
#include <stdint.h>
#include <stdio.h>


#include "lodepng.h"
#include "helpers.h"

extern int errno;

#define X 0
#define Y 1
#define DIST 2

#define FNAME_MAX_LEN 255

#define SQUARE(a) ((a) * (a))


void require_file(FILE* f, char* fname) {
    if(f == NULL) {
        fprintf(stderr, "%s: %s\n", fname, strerror(errno));
        exit(errno);
    }
}

// --------------- Images ---------------
float** malloc_f32matrix(int w, int h) {
    float** image = malloc(sizeof(float*) * h);
    image[0] = (float*) calloc(h * w, sizeof(float));

    for(int i=0; i < h; i++) {
        image[i] = (*image + w * i);
    }

    return image;
}

void free_f32matrix(float** image) {
    free(image[0]);
    free(image);
}

uint16_t** malloc_uint16matrix(int dim2, int dim1) {
    uint16_t** image = (uint16_t**) malloc(sizeof(uint16_t*) * dim1);
    assert(image != NULL);

    image[0] = (uint16_t*) calloc(dim1 * dim2, sizeof(uint16_t));
    assert(image[0] != NULL);

    for(int i=0; i < dim1; i++) {
        image[i] = (uint16_t*) (*image + dim2 * i);
    }

    return image;
}

void free_uint16matrix(uint16_t** image) {
    free(image[0]);
    free(image);
}

// TODO : (w, h, k)  is a bad convention
float*** malloc_f32cube(int dim1, int dim3, int dim2) {
    float*** cube = malloc(sizeof(float**) * dim1);

    for(int i=0; i<dim1; i++) {
        cube[i] = malloc_f32matrix(dim3, dim2);
    }

    return cube;
}

void free_f32cube(float*** cube, int k) {

    for(int i=0; i < k; i++) {
        free_f32matrix(cube[i]);
    }

    free(cube);
}

void read_image_header(FILE* f, int *w, int *h, int *size) {
    char foo[100];

    // Magic number (P5, P6)
    fgets(foo, 100, f);

    // "w h" or "#" (comment)
    fgets(foo, 100, f);

    while(foo[0] == '#')
        fgets(foo, 100, f);

    sscanf(foo, "%d %d", w, h);

    fgets(foo, 100, f);

    sscanf(foo, "%d", size);
}

float** load_pgm(char* name, int *w, int *h) {
    int i, j, v, size;
    float** mat;
    FILE *f = fopen(name, "r");

    require_file(f, name);

    read_image_header(f, w, h, &size);

    size = size == 65535 ? 2 : 1;

    // Read
    mat = malloc_f32matrix(*w, *h);

    for(i=0; i < *h; i++)
        for(j=0; j < *w; j++) {
            if(size == 2) {
                // Higher
                v = getc(f) << 8;
                // Lower
                v += getc(f);
            } else {
                v = getc(f);
            }

            mat[i][j] = v;
        }

    fclose(f);

    return mat;
}

float*** load_ppm(char* name, int *w, int *h) {

    int i, j, k, v, size;
    float*** mat = malloc(sizeof(float**) * 3); // R,G,B channels

    FILE *f = fopen(name, "r");

    require_file(f, name);

    read_image_header(f, w, h, &size);

    size = size == 65535 ? 2 : 1;

    // Read
    for(i=0; i<3; i++)
        mat[i] = malloc_f32matrix(*w, *h);

    for(i=0; i < *h; i++)
        for(j=0; j < *w; j++) {
            for(k=0; k<3; k++) {
                if(size == 2) {
                    // Higher
                    v = getc(f) << 8;
                    // Lower
                    v += getc(f);
                } else {
                    v = getc(f);
                }

                mat[k][i][j] = v;
            }
        }

    fclose(f);

    return mat;
}

void save_pgm(char* filename, float** image, int w, int h, int depth) {
    int i, j, v;

    int max = depth == 16 ? 65535 : 255;

    FILE* out;
    if(filename != NULL) {
        out = fopen(filename, "w+");
	} else {
        out = stdout;
	}

    fprintf(out, "P5\n#\n%d %d\n%d\n", w, h, max);
    for(i=0; i < h; i++) {
        for(j=0; j<w; j++) {

            v = image[i][j];

            if(v > max || v < 0) {
                fprintf(stderr, "*** WARNING : (%d, %d) = %f\n", i, j, image[i][j]);
                v = fmin(fmax(v, 0), max);
            }

            if(depth == 16) {
                // Higher
                fputc((v >> 8), out);
                // Lower
                fputc((v & 255), out);
            } else {
                fputc(v, out);
            }
        }
    }

    fclose(out);
}

void save_ppm(char* filename, float** channels[3], int w, int h, int depth) {
    int i, j, k, v;
    int max = depth == 16 ? 65535 : 255;

    FILE* out;

    if(filename != NULL) {
        out = fopen(filename, "w+");
	} else {
        out = stdout;
	}

    fprintf(out, "P6\n#\n%d %d\n%d\n", w, h, max);
    for(i=0; i < h; i++) {
        for(j=0; j<w; j++) {

            for(k=0; k<3; k++) {
                v = channels[k][i][j];

                if(v > max || v < 0) {
                    fprintf(stderr, "*** WARNING : (%d, %d) = %f [channel=%d]\n", i, j, channels[k][i][j], k);
                    v = fmin(fmax(v, 0), max);
                }

                if(depth == 16) {
                    // Higher
                    fputc((v >> 8), out);
                    // Lower
                    fputc((v & 255), out);
                } else {
                    fputc(v, out);
                }
            }
        }
    }

    fclose(out);
}

unsigned char* load_png(char* fname, int *w, int *h, int *depth, LodePNGColorType expected_type) {

    unsigned int error;
    unsigned char* image;
    unsigned int width, height;
    unsigned char* png = 0;
    size_t pngsize;

    LodePNGState state;

    lodepng_state_init(&state);

    state.info_raw.colortype = expected_type;

    error = lodepng_load_file(&png, &pngsize, fname);

    lodepng_inspect(&width, &height, &state, png, pngsize);

    *depth = (int) state.info_png.color.bitdepth;
    state.info_raw.bitdepth = state.info_png.color.bitdepth;

    if(!error)
        error = lodepng_decode(&image, &width, &height, &state, png, pngsize);

    if(state.info_png.color.colortype != expected_type) {
        fprintf(stderr, "%s: color type is not %s\n", fname, expected_type == LCT_RGB ? "RGB" : "Grayscale");
        exit(-1);
    }

    if(error) {
        fprintf(stderr, "error %u: %s\n", error, lodepng_error_text(error));
        exit(error);
    }

    *w = (int) width;
    *h = (int) height;

    free(png);
    lodepng_state_cleanup(&state);

    return image;
}

float** load_png_gray(char* fname, int *w, int *h) {

    int depth;

    unsigned char* image = load_png(fname, w, h, &depth, LCT_GREY);

    float** array = malloc_f32matrix(*w, *h);

    unsigned int n = 0;
    for(int i=0; i<*h; i++)
        for(int j=0; j<*w; j++) {
            int val = image[n++];

            if(depth == 16)
                val = (val << 8) + image[n++];

            array[i][j] = val;
        }

    free(image);

    return array;
}

float*** load_png_rgb(char* fname, int *w, int *h) {

    int depth;

    unsigned char* image = load_png(fname, w, h, &depth, LCT_RGB);

    float*** array = malloc_f32cube(3, *w, *h);
    unsigned int n = 0;
    for(int i=0; i<*h; i++)
        for(int j=0; j<*w; j++) {
            for(int k=0; k<3; k++) {
                int val = image[n++];

                if(depth == 16)
                    val = (val << 8) + image[n++];

                array[k][i][j] = val;
            }
        }

    free(image);

    return array;
}

char is_png(char* fname) {

    // Ensures file exists
    FILE* f = fopen(fname, "r");
    require_file(f, fname);
    fclose(f);

    int end = strlen(fname) - 4;

    if(end < 0)
        end = 0;

    return strcmp(fname + end, ".png") == 0;
}

float*** load_color(char* fname, int *w, int *h) {

    if(is_png(fname))
        return load_png_rgb(fname, w, h);

    return load_ppm(fname, w, h);
}

float** load_gray(char* fname, int *w, int *h) {

    if(is_png(fname))
        return load_png_gray(fname, w, h);

    return load_pgm(fname, w, h);
}

void save_raw_png(char* filename, unsigned char* image, int w, int h, int depth, LodePNGColorType colortype) {

    unsigned int width = w;
    unsigned int height = h;

    unsigned int error;
    unsigned char* png;
    size_t pngsize;
    LodePNGState state;

    lodepng_state_init(&state);

    state.encoder.auto_convert = 0;

    state.info_raw.colortype = colortype;
    state.info_raw.bitdepth = depth;

    state.info_png.color.colortype = colortype;
    state.info_png.color.bitdepth = depth;

    error = lodepng_encode(&png, &pngsize, image, width, height, &state);

    if(!error)
        error = lodepng_save_file(png, pngsize, filename);

    if(error) {
        fprintf(stderr, "error %u: %s\n", error, lodepng_error_text(error));
    }

    lodepng_state_cleanup(&state);
    free(png);
}

void save_gray_png(char* filename, float** image, int w, int h, int depth) {

    unsigned char* array = malloc(w * h * (depth == 16 ? 2 : 1));

    unsigned int n = 0;

    int max = depth == 16 ? 65535 : 255;

    for(int i=0; i<h; i++)
        for(int j=0; j<w; j++) {
                int val = image[i][j];

                if(val > max || val < 0) {
                    fprintf(stderr, "*** WARNING : (%d, %d) = %f\n", i, j, image[i][j]);
                    val = fmin(fmax(val, 0), max);
                }

                if(depth == 16) {
                    array[n++] = val >> 8;
                }

                array[n++] = val & 255;
        }

    save_raw_png(filename, array, w, h, depth, LCT_GREY);

    free(array);
}

void save_color_png(char* filename, float*** image, int w, int h, int depth) {

    unsigned char* array = malloc(w * h * 3 * (depth == 16 ? 2 : 1));

    unsigned int n = 0;

    int max = depth == 16 ? 65535 : 255;

    for(int i=0; i<h; i++)
        for(int j=0; j<w; j++) {
            for(int k=0; k<3; k++) {
                int val = image[k][i][j];

                if(val > max || val < 0) {
                    fprintf(stderr, "*** WARNING : (%d, %d) = %f [channel=%d]\n", i, j, image[k][i][j], k);
                    val = fmin(fmax(val, 0), max);
                }

                if(depth == 16) {
                    array[n++] = val >> 8;
                }

                array[n++] = val & 255;
            }
        }

    save_raw_png(filename, array, w, h, depth, LCT_RGB);

    free(array);
}

// --------------- Misc calculations ---------------

int* random_numbers(int size, int total) {

    assert(size <= total);

    int i, j, swap;
    int* list = malloc(sizeof(int) * total);

    for(i=0; i<total; i++)
        list[i] = i;


    // Shuffle
    for(i=0; i<total; i++) {
        j = rand() % total;
        swap = list[i];
        list[i] = list[j];
        list[j] = swap;
    }

    int* out = malloc(sizeof(int) * size);

    for(i=0; i<size; i++) {
        out[i] = list[i];
    }

    free(list);

    return out;
}

float solve_phase_term(float* intensities, int nb_shifts, float (*trigo)(float)) {
    float total = 0;

    for(int i=0; i<nb_shifts; i++) {
        total += trigo(2 * i * M_PI /(float) nb_shifts) * intensities[i];
    }

    return total;
}

float solve_phase(float* intensities, int nb_shifts) {
    return atan2(solve_phase_term(intensities, nb_shifts, sinf),
                 solve_phase_term(intensities, nb_shifts, cosf));
}

/**
 * Bilinear interpolation of four values
 *
 * 0,0       1,0
 *   |---|---|
 *   | a | b | |
 *   |---|---| | v
 *   | c | d | v
 *   |---|---|
 * 0,1  ---> 1,1
 *       u
 */
float billy(float u, float v, float a, float b, float c, float d) {
    return (1 - v) * ((1 - u) * a + u * b) + v * ((1 - u) * c + u * d);
}

/**
 * Compute the continuous cost between a camera and reference code.
 *
 * See Eq (9) of Subpixel Unsynchronized Unstructured Light, Chaima El
 * Asmi, Sébastien Roy
 */
float pixel_cost(float* cam_code, float* ref_code, int nb_bits) {

    float dot_product = 0;
    float norm_cam = 0;
    float norm_ref = 0;

    for(int k=0; k<nb_bits; k++) {
        dot_product += ref_code[k] * cam_code[k];
        norm_cam += SQUARE(cam_code[k]);
        norm_ref += SQUARE(ref_code[k]);
    }

    norm_cam = sqrt(norm_cam);
    norm_ref = sqrt(norm_ref);

    // Check for NAN
    if(norm_cam != norm_cam || norm_ref != norm_ref) {
        return 1;
    }

    if(!(fabs(norm_cam - 1) < 1e-6 ||
         fabs(norm_cam - 1) > 1e-6 ||
         fabs(norm_ref - 1) > 1e-6 ||
         fabs(norm_ref - 1) < 1e-6)) {
        printf("*** WARNING: norm should be one, but is %f %f\n", norm_cam, norm_ref);
    }

    dot_product /= norm_ref * norm_cam;

    if(!(dot_product >= -1.0 - 1e-6 && dot_product <= 1.0 + 1e-6)) {
        printf("Warning: dot product is not between -1 and 1: %6.12f in pixel_cost\n", dot_product);
    }
    dot_product = fmin(1, fmax(dot_product, -1));

    return (1 - dot_product) / 2.0;
}

float code_avg(float* code, int nb_bits) {
    float avg = 0;
    for(int i=0; i<nb_bits; i++) {
        avg += code[i] / nb_bits;
    }
    return avg;
}

float code_norm(float* code, int nb_bits) {
    float norm = 0;
    for(int i=0; i<nb_bits; i++) {
        norm += SQUARE(code[i]);
    }
    return sqrt(norm);
}

void normalize_codes(float*** codes, int w, int h, int nb_bits) {
    // Normalize to codes
    for(int i=0; i<h; i++)
        for(int j=0; j<w; j++) {
            float avg = code_avg(codes[i][j], nb_bits);

            for(int n=0; n<nb_bits; n++) {
                codes[i][j][n] -= avg;
            }

            float norm = code_norm(codes[i][j], nb_bits);

            for(int n=0; n<nb_bits; n++) {
                codes[i][j][n] /= norm;
            }
        }
}

float*** load_continuous_codesq(char* format, int width, int height, int nb_patterns) {

    char filename[FNAME_MAX_LEN];
    int w, h;
    int nb_bits = nb_patterns * (nb_patterns - 1) / 2;

    float*** codes = malloc_f32cube(height, nb_bits, width);

    int nb_copies = nb_patterns - 1;
    int copy_start = 0;

    // Camera binary codes
    for(int k=0; k<nb_patterns; k++) {

        // Load first cam image
        sprintf(filename, format, k);
        float** img = load_gray(filename, &w, &h);

        /* printf("k=%d; img[0][0] == %f\n", k, img[0][0] / 255.0); */

        assert(w == width && h == height);

        // Copy image values
        for(int i=0; i<height; i++)
            for(int j=0; j<width; j++) {
                for(int n=0; n<nb_copies; n++) {
                    // printf("copy k=%d to %d\n", k, copy_start + n);
                    codes[i][j][copy_start + n] = -img[i][j] / 255.0;
                }
            }

        // Sub to the previously copied value
        for(int i=0; i<height; i++)
            for(int j=0; j<width; j++) {
                int idx = k - 1;
                int jumps = nb_patterns - 2;

                for(int n=0; n<k; n++) {
                    // printf("sub k=%d to %d\n", k, idx);
                    codes[i][j][idx] += img[i][j] / 255.0;

                    idx += jumps;
                    jumps--;
                }
            }

        copy_start += nb_copies;
        nb_copies--;
    }

    normalize_codes(codes, width, height, nb_bits);

    return codes;
}

void load_continuous_codes(float*** codes, char* format,
                           int width, int height, int nb_patterns,
                           int quadratic) {
    char filename[FNAME_MAX_LEN];
    int w, h, n=0;

    // Camera binary codes
    for(int k=0; k<nb_patterns - 1; k++) {

        // Load first cam image
        float** previous_image;

        sprintf(filename, format, k);
        previous_image = load_gray(filename, &w, &h);

        assert(w == width && h == height);

        float** current_image;

        // Load next images
        for(int l=k + 1; l<nb_patterns; l++, n++) {

            sprintf(filename, format, l);
            current_image = load_gray(filename, &w, &h);

            assert(w == width && h == height);

            for(int i=0; i<height; i++)
                for(int j=0; j<width; j++) {

                    float diff = current_image[i][j] / 255.0 - previous_image[i][j] / 255.0;

                    codes[i][j][n] = diff;
                }

            free_f32matrix(current_image);

            if(!quadratic) {
                n++;
                break;
            }
        }

        // Free last image
        free_f32matrix(previous_image);
    }

    normalize_codes(codes, width, height, n);
}

void load_intensities(float*** codes, char* format,
                      int width, int height, int nb_patterns, int normalize) {
    char filename[FNAME_MAX_LEN];
    int w, h;

    // Camera binary codes
    for(int k=0; k<nb_patterns; k++) {

        // Load first cam image
        float** img;

        sprintf(filename, format, k);
        img = load_gray(filename, &w, &h);

        assert(w == width && h == height);

        for(int i=0; i<height; i++)
            for(int j=0; j<width; j++) {
                codes[i][j][k] = img[i][j];
            }
        free_f32matrix(img);
    }

    if(normalize)
        normalize_codes(codes, width, height, nb_patterns);
}

float** load_mask(char* img_format, int nb_patterns, int w, int h) {

    char* mask_fname = "maskCam.png";

    float** mask = malloc_f32matrix(w, h);
    float** mask_max = malloc_f32matrix(w, h);
    float** mask_min = malloc_f32matrix(w, h);

    #pragma omp parallel for
    for(int i=0; i < h; i++)
        for(int j=0; j < w; j++)
            mask_min[i][j] = 255;

    // Compute mask
    #pragma omp parallel for
    for(int k=0; k<nb_patterns; k++) {

        char filename[FNAME_MAX_LEN];
        sprintf(filename, img_format, k);

        float** image = load_gray(filename, &w, &h);

        #pragma omp parallel for
        for(int i=0; i < h; i++) {
            for(int j=0; j < w; j++) {
                mask_min[i][j] = fmin(mask_min[i][j], image[i][j]);
                mask_max[i][j] = fmax(mask_max[i][j], image[i][j]);
            }
        }

        free_f32matrix(image);
    }

    #pragma omp parallel for
    for(int i=0; i < h; i++) {
        for(int j=0; j < w; j++) {
            mask[i][j] = (int) mask_max[i][j] - mask_min[i][j];
        }
    }

    save_gray_png(mask_fname, mask, w, h, 8);
    save_gray_png("camMin.png", mask_min, w, h, 8);
    save_gray_png("camMax.png", mask_max, w, h, 8);

    return mask;
}

/**
 * Remap an array of x/y/error to an array of 16-bit integers and save it
 */
void save_color_map(char* filename, float*** matches, int from_w, int from_h,
                    int to_w, int to_h, float max_distance) {
    float*** channels = malloc_f32cube(3, from_w, from_h);

    // Color map
    for(int i=0; i<from_h; i++)
        for(int j=0; j<from_w; j++) {
            if(matches[X][i][j] == -1.0) {
                channels[X][i][j] = 65535.0;
                channels[Y][i][j] = 65535.0;
                channels[DIST][i][j] = 65535.0;
            } else {
                assert(matches[DIST][i][j] <= max_distance);
                assert(matches[DIST][i][j] >= -1e-6);

                matches[DIST][i][j] = fmax(matches[DIST][i][j], 0);

                channels[X][i][j] = matches[X][i][j] * 65535.0/(to_w - 1);
                channels[Y][i][j] = matches[Y][i][j] * 65535.0/(to_h - 1);
                channels[DIST][i][j] = fmin(matches[DIST][i][j] * 65535.0/max_distance, 65535.0);
            }
        }

    save_color_png(filename, channels, from_w, from_h, 16);

    free_f32cube(channels, 3);
}
